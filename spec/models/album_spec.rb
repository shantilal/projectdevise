require 'spec_helper'

describe Album do
	it "valid with userid ,album_name and visibility" do
		
		user=User.create(username:"bhavin",location:"surat",email:"testing@gmail.com",password:"123456789",password_confirmation: "123456789")

		album=user.albums.new(user_id: 1,album_name: "lalo",visibility: "public")

		expect(album).to be_valid
	end

	it "is invallid without user_id " do

		expect(Album.new(user_id: nil)).to have(1).error_on(:user_id)
	end

	it "is invallid without album_name " do
		expect(Album.new(album_name: nil)).to have(1).error_on(:album_name)
	end

	it "is invallid without visibility" do
		expect(Album.new(visibility: nil)).to have(1).error_on(:visibility)
	end


end
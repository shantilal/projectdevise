require 'spec_helper'
describe User do

	it "is valid with a name,locaion and email" do
		#user=User.new(username:"bhavin",location:"surat",email:"bhavin@gmail.com",password:"123456789",password_confirmation: "123456789")
	#	expect(user).to be_valid
	#	expect(FactoryGril.build(:user)).to be_valid
	expect(FactoryGirl.build(:user)).to be_valid
	end
	it "is invalid without a username" do	
		expect(User.new(username: nil)).to have(1).errors_on(:username)
	end

	it "is invalid without an email address" do
		expect(User.new(email: nil)).to have(1).errors_on(:email)
	end
	
	it "is invalid with a duplicate email address" do
		User.create(username:"bhavin",location:"surat",email:"testing@gmail.com",password:"123456789",password_confirmation: "123456789")
		user=User.new(username:"bhavin",location:"surat",email:"testing@gmail.com",password:"123456789",password_confirmation: "123456789")
		expect(user).to have(1).errors_on(:email)
	end


end
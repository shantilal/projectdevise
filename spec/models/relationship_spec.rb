require 'spec_helper'

describe Relationship do
	it "it should valid follondwing and followers id " do
		relationship=Relationship.new(follower_id: 1 ,followed_id: 2)
		expect(relationship).to be_valid 
	end

	it "is invalid without followed_id " do
		expect(Relationship.new(followed_id: nil)).to have(1).error_on(:followed_id)
	end

	it "is invalid without follower_id " do
		expect(Relationship.new(follower_id: nil)).to have(1).error_on(:follower_id)
	end


	it "following and followers id is not same" do
		relationship=Relationship.new(follower_id:1 ,followed_id:1)
		expect(relationship).to be_valid
	end
end
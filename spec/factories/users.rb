FactoryGirl.define do
	factory :user do
		username "bhavin"
		location "surat"
		sequence(:email) {|n| "bhavin#{n}@example.com"}
		password "123456789"
		password_confirmation "123456789"
	end	
end

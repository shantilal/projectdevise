require 'test_helper'

class UserSignupTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

   test "invalid signup information" do
    get new_user_registration_path
    assert_no_difference 'User.count' do
      post user_registration_path, user: { 
                               email: "user@invalid",
                               password:              "foo",
                               password_confirmation: "bar" }
    end
 
    assert_template 'devise/registrations/new'
  
  end

   test "valid signup information" do
    get  new_user_registration_path
    assert_difference 'User.count', 1 do
      post_via_redirect user_registration_path, user: {
                                            email: "user@example.com",
                                            password:              "password",
                                            password_confirmation: "password",
                                            provider:"facebook",
                                            uid:"sdshdfshdfshdfwwe123" }
    end
    assert_template 'devise/registrations/new'
 
  end
  
end

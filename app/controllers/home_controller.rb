class HomeController < ApplicationController
    def new
		@q = User.ransack(params[:q])
	end

	def index
		@q = User.ransack(params[:q])
		@user = @q.result
	end

	def show
		@user=User.friendly.find(params[:id])



#		@album=Album.where(:user_id => @user.id,:visibility => "public")

		@album=Album.public_user_albums(@user)	

        @following =@user.following

        @follower=@user.followers


        @friend=@following & @follower


       # @friend_album=Album.where(:user_id => @user.following,:visibility => "friend")

       	@friend_album=Album.public_user_friend(@friend)
	end

end

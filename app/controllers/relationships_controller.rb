class RelationshipsController < ApplicationController
  
  def create
    user = User.find((params[:followed_id]).to_i)
    current_user.follow(user)
    redirect_to home_path(user)
  end

  def destroy
    user = Relationship.find((params[:id]).to_i).followed
    current_user.unfollow(user)
    redirect_to home_path(user)
  end
  
end

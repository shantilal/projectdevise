class AlbumsController < ApplicationController
	
	def index
		@album=current_user.albums.all
	end



	def new
		@album=Album.new
		@album.images.build
	
  	end
	
	def create 
		 @album = current_user.albums.build(albums_params)
	     if @album.save
    		    flash[:success] = "album created!"
      		    redirect_to albums_path    			
    	end

	end
	
	def show
		@images=Image.where(:album_id => params[:id])		
	end
	
	def destory
	
	end


  private

    def albums_params
      params.require(:album).permit(:album_name,:visibility,images_attributes: [:avatar, :_destroy])
    end

end

class Album < ActiveRecord::Base
  belongs_to :user
  
  has_many :images ,dependent: :destroy

  accepts_nested_attributes_for :images,:allow_destroy => true 

  validates :user_id, presence: true
  validates :album_name, presence: true
  validates :visibility, presence: true

  def self.public_user_albums(user)
  		Album.where(:user_id => user.id,:visibility => "public")
  end

  def self.public_user_friend(user)
  		Album.where(:user_id => user,:visibility => "friend")
  end

end

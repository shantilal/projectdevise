class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.references :album, index: true

      t.timestamps
    end
    add_index :images, [:album_id, :created_at]
  end
end
